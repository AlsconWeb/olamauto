<?php
/**
 * Index theme file.
 *
 * @package olamauto/theme
 */

use Olamauto\FrontHelper;

get_header();
?>
	<section>
		<div class="container">
			<div class="row">
				<div class="col">
					<?php echo wp_kses_post( crb_get_i18n_theme_option( 'ao_mian_text_block' ) ?? '' ); ?>
					<a class="icon-arrow-down" href="#tabs">
						<?php esc_attr_e( 'Подробнее', 'olamauto' ); ?>
					</a>
					<img
							class="car"
							src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/shark.png' ); ?>"
							alt="Car">
				</div>
			</div>
			<div class="row">
				<div class="col">
					<div class="tabs" id="tabs">
						<div class="tab active" id="form-tab">
							<form id="your-car" method="post">
								<h2><?php esc_attr_e( 'Узнайте, сколько предварительно стоит ваш автомобиль', 'olamauto' ); ?></h2>
								<div class="select">
									<?php FrontHelper::show_model_select(); ?>
								</div>
								<div class="input">
									<input
											type="text"
											class="color"
											name="oa_color"
											id="oa-color"
											data-placeholder="<?php esc_attr_e( 'Цвет', 'olamauto' ); ?>"
											placeholder="<?php esc_attr_e( 'Цвет', 'olamauto' ); ?>">
								</div>
								<div class="select">
									<select
											class="year"
											name="oa_year"
											id="oa-year"
											data-placeholder="<?php esc_attr_e( 'Год', 'olamauto' ); ?>">
										<option></option>
									</select>
								</div>
								<div class="select">
									<input
											type="text"
											class="mileage"
											name="oa_mileage"
											id="oa-mileage"
											data-placeholder="<?php esc_attr_e( 'Пробег', 'olamauto' ); ?>"
											placeholder="<?php esc_attr_e( 'Пробег', 'olamauto' ); ?>">
								</div>
								<div class="select">
									<?php FrontHelper::show_state_select(); ?>
								</div>
								<div class="select">
									<?php FrontHelper::show_transmission_select(); ?>
								</div>
								<div class="dfr">
									<input type="reset" value="<?php esc_attr_e( 'Сбросить', 'olamauto' ); ?>">
									<input
											class="button"
											type="submit"
											value="<?php esc_attr_e( 'Рассчитать стоимость', 'olamauto' ); ?>">
								</div>
							</form>
						</div>
						<div class="tab" id="result-tab">
							<?php echo wp_kses_post( crb_get_i18n_theme_option( 'ao_result_price' ) ); ?>
							<div class="dfr center">
								<a class="button back" id="first-step" href="#">
									<?php esc_attr_e( 'Вернуться', 'olamauto' ); ?>
								</a>
								<a class="button" id="lead-form-step" href="#">
									<?php esc_attr_e( 'Узнать точную стоимость', 'olamauto' ); ?>
								</a>
							</div>
						</div>
						<div class="tab" id="lead-tab">
							<?php echo wp_kses_post( crb_get_i18n_theme_option( 'ao_lead_form' ) ); ?>
							<form action="" id="lead-forms">
								<div class="input fw">
									<input
											type="text"
											name="fio"
											id="lead-fio"
											placeholder="<?php esc_attr_e( 'ФИО', 'olamauto' ); ?>"
											required>
								</div>
								<div class="input">
									<input
											type="email"
											name="email"
											id="lead-email"
											placeholder="<?php esc_attr_e( 'Электронная почта', 'olamauto' ); ?>"
											required>
								</div>
								<div class="input min">
									<input
											type="tel"
											name="phone"
											id="lead-phone"
											placeholder="<?php esc_attr_e( 'Номер телефона', 'olamauto' ); ?>"
											required>
								</div>
								<div class="dfr center">
									<a class="button back" id="back-to-price" href="#">
										<?php esc_attr_e( 'Вернуться', 'olamauto' ); ?>
									</a>
									<input
											class="button"
											type="submit"
											value="<?php esc_attr_e( 'Обратная связь', 'olamauto' ); ?>">
								</div>
							</form>
						</div>
						<div class="tab" id="thx-tab">
							<?php echo wp_kses_post( crb_get_i18n_theme_option( 'ao_tnx_page' ) ); ?>
							<div class="dfr center">
								<a class="button back" id="back-to-price" href="#">
									<?php esc_attr_e( 'Вернуться', 'olamauto' ); ?>
								</a>
								<a class="button" href="https://repost.uz/special/olamavto/">
									<?php esc_attr_e( 'Вернуться на начало', 'olamauto' ); ?>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
