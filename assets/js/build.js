'use strict';

jQuery(document).ready(function ($) {
    //select
    var selectEl = $('select');
    if ($('select').length) {
        // select reset
        selectEl.each(function () {
            $(this).val(null).trigger('change');
        });
        selectEl.each(function () {
            $(this).select2({
                dropdownParent: $(this).parent()
            });
        });
        // select car change
        $('.modal').change(function () {
            var nameModal = $(this).val().replace(/ /g, '');
            $('.car').css('transform', 'translateX(150%)');
            setTimeout(function () {
                $('.car').attr('src', '/special/olamavto/wp-content/themes/olamauto/assets/img/' + nameModal + '.png');
            }, 200);
            setTimeout(function () {
                $('.car').css('transform', 'translateX(0%)');
            }, 600);
        });
    }
    // reset form
    $('[type="reset"]').click(function () {
        selectEl.each(function () {
            $(this).val(null).trigger('change');
        });
    });

    //scroll block
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
