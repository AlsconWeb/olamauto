/* global oaMainObject, jQuery */

/**
 * @param oaMainObject.ajaxUrl
 * @param oaMainObject.changeModelAction
 * @param oaMainObject.changeModelNonce
 * @param oaMainObject.youCarFormAction
 * @param oaMainObject.youCarFormNonce
 * @param oaMainObject.sendLeadAction
 * @param oaMainObject.sendLeadNonce
 */

jQuery( document ).ready( function( $ ) {

	/**
	 * Get year by model.
	 */
	const modelSelect = $( '#oa-model' );
	if ( modelSelect.length ) {
		modelSelect.change( function( e ) {

			const data = {
				action: oaMainObject.changeModelAction,
				changeModelNonce: oaMainObject.changeModelNonce,
				model: $( this ).val()
			};

			const yearSelect = $( '#oa-year' );

			$.ajax( {
				type: 'POST',
				url: oaMainObject.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						$( '#oa-year option' ).remove();
						yearSelect.append( res.data.options );
					}
				},
				error: function( xhr ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	/**
	 * Send form.
	 */
	const yuoCarFrom = $( '#your-car' );
	if ( yuoCarFrom.length ) {
		yuoCarFrom.submit( function( e ) {
			e.preventDefault();

			const data = {
				action: oaMainObject.youCarFormAction,
				yuoCarFromNonce: oaMainObject.youCarFormNonce,
				model: $( '#oa-model' ).val(),
				year: $( '#oa-year' ).val()
			};

			$.ajax( {
				type: 'POST',
				url: oaMainObject.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						const carData = {
							model: data.model,
							year: data.year,
							color: $( '#oa-color' ).val(),
							mileage: $( '#oa-mileage' ).val(),
							state: $( '#oa-state' ).val(),
							transmission: $( '#oa-transmission' ).val(),
							price: res.data.price
						};
						localStorage.setItem( 'yuo-car', JSON.stringify( carData ) );
						setSum();
						$( '#form-tab' ).removeClass( 'active' );
						$( '#result-tab' ).addClass( 'active' );
					}
				},
				error: function( xhr ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	/**
	 * Set sum.
	 */
	let priceEl = $( '.price' );
	if ( priceEl.length && localStorage.getItem( 'yuo-car' ) ) {
		setSum();
	}

	function setSum() {
		let carData = JSON.parse( localStorage.getItem( 'yuo-car' ) );

		let price = new Intl.NumberFormat( 'uz-UZ' ).format( Number( carData.price ) );

		priceEl.text( price + ' сум' );
	}

	/**
	 * Back to first step.
	 */
	const backToFirstStep = $( '#first-step' );
	if ( backToFirstStep.length ) {
		backToFirstStep.click( function( e ) {
			e.preventDefault();

			$( '#result-tab' ).removeClass( 'active' );
			$( '#form-tab' ).addClass( 'active' );
		} );
	}

	/**
	 * Get lead form step.
	 */
	const leadFromStep = $( '#lead-form-step' );
	if ( leadFromStep.length ) {
		leadFromStep.click( function( e ) {
			e.preventDefault();

			$( '#result-tab' ).removeClass( 'active' );
			$( '#lead-tab' ).addClass( 'active' );
		} );
	}

	/**
	 * Back to price
	 */
	const backToPrice = $( '#back-to-price' );
	if ( backToPrice.length ) {
		backToPrice.click( function( e ) {
			e.preventDefault();

			$( '#lead-tab' ).removeClass( 'active' );
			$( '#result-tab' ).addClass( 'active' );
		} );
	}

	/**
	 * Send Lead.
	 */
	const leadForm = $( '#lead-forms' );
	if ( leadForm.length ) {
		leadForm.submit( function( e ) {
			e.preventDefault();

			const learCarData = JSON.parse( localStorage.getItem( 'yuo-car' ) );

			const data = {
				action: oaMainObject.sendLeadAction,
				sendLeadNonce: oaMainObject.sendLeadNonce,
				fullName: $( '#lead-fio' ).val(),
				email: $( '#lead-email' ).val(),
				leadPhone: $( '#lead-phone' ).val(),
				model: learCarData.model,
				year: learCarData.year,
				color: learCarData.color,
				mileage: learCarData.mileage,
				state: learCarData.state,
				transmission: learCarData.transmission,
				price: learCarData.price
			};

			$.ajax( {
				type: 'POST',
				url: oaMainObject.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						$( '#lead-tab' ).removeClass( 'active' );
						$( '#thx-tab' ).addClass( 'active' );
					} else {
						alert( res.data.message );
					}
				},
				error: function( xhr ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	/**
	 * Reset button.
	 */
	const resetBtn = $( '.tab input[type=reset]' );
	if ( resetBtn.length ) {
		resetBtn.click( function( e ) {
			localStorage.removeItem( 'yuo-car' );
		} );
	}
} );
