/* global oaAdminMainObj, jQuery */

/**
 * @param oaAdminMainObj.ajaxUrl
 * @param oaAdminMainObj.addCarAction
 * @param oaAdminMainObj.addCarNonce
 * @param oaAdminMainObj.deleteCarAction
 * @param oaAdminMainObj.deleteCarNonce
 */
jQuery( document ).ready( function( $ ) {

	/**
	 * Add car.
	 */
	const addCarForm = $( '#add_car' );
	if ( addCarForm.length ) {
		addCarForm.submit( function( e ) {
			e.preventDefault();

			const data = {
				action: oaAdminMainObj.addCarAction,
				addCarNonce: oaAdminMainObj.addCarNonce,
				model: $( '#oa-model' ).val(),
				year: $( '#oa-year' ).val(),
				price: $( '#oa-price' ).val()
			};

			$.ajax( {
				type: 'POST',
				url: oaAdminMainObj.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						window.location.reload();
					} else {
						alert( res.data.message );
					}
				},
				error: function( xhr ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

	/**
	 * Delete car.
	 */
	const deleteCar = $( '.table .delete' );
	if ( deleteCar.length ) {
		deleteCar.click( function( e ) {
			e.preventDefault();

			const data = {
				action: oaAdminMainObj.deleteCarAction,
				deleteCarNonce: oaAdminMainObj.deleteCarNonce,
				id: $( this ).data( 'id' )
			};

			$.ajax( {
				type: 'POST',
				url: oaAdminMainObj.ajaxUrl,
				data: data,
				success: function( res ) {
					if ( res.success ) {
						window.location.reload();
					} else {
						alert( res.data.message );
					}
				},
				error: function( xhr, ajaxOptions, thrownError ) {
					console.log( 'error...', xhr );
					//error logging
				}
			} );
		} );
	}

} );
