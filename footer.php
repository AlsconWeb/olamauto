<?php
/**
 * Footer template.
 *
 * @package olamauto/theme
 */

$repost_footer_logo_id  = carbon_get_theme_option( 'oa_footer_logo_repost' );
$repost_footer_logo_url = wp_get_attachment_image_url( $repost_footer_logo_id, 'full' );
$social_media           = carbon_get_theme_option( 'oa_social' );

?>
<footer>
	<div class="container">
		<div class="row align-items-center">
			<div class="col-auto">
				<p class="copyright">
					<?php echo wp_kses_post( carbon_get_theme_option( 'oa_copyright' ) ?? '' ); ?>
				</p>
			</div>
			<div class="col-auto">
				<?php if ( ! empty( $repost_footer_logo_id ) ) { ?>
					<a class="logo" href="http://repost.uz/">
						<img src="<?php echo esc_url( $repost_footer_logo_url ); ?>" alt="repost-logo">
					</a>
				<?php } ?>
			</div>
			<div class="col">
				<?php if ( ! empty( $social_media ) ) { ?>
					<ul class="soc">
						<?php foreach ( $social_media as $media ) { ?>
							<li class="icon-<?php echo esc_attr( $media['name'] ?? '' ); ?>">
								<a href="<?php echo esc_url( $media['url'] ?? '' ); ?>"></a>
							</li>
						<?php } ?>
					</ul>
				<?php } ?>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
</body>
</html>

