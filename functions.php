<?php
/**
 * Functions theme olamauto.
 *
 * @package olamauto/theme
 */

use Olamauto\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();

function crb_get_i18n_suffix() {
	$suffix = '';
	if ( ! defined( 'ICL_LANGUAGE_CODE' ) ) {
		return $suffix;
	}
	$suffix = '_' . ICL_LANGUAGE_CODE;

	return $suffix;
}

function crb_get_i18n_theme_option( $option_name ) {
	$suffix = crb_get_i18n_suffix();

	return carbon_get_theme_option( $option_name . $suffix );
}
