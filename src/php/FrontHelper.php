<?php
/**
 * Front end helpers.
 *
 * @package olamauto/theme
 */

namespace Olamauto;

/**
 * FrontHelper class name.
 */
class FrontHelper {

	/**
	 * Show model select.
	 *
	 * @return void
	 */
	public static function show_model_select(): void {
		$models = DBHelper::get_unique_model();

		if ( ! empty( $models ) ) {
			?>
			<select
					class="modal"
					name="oa_model"
					id="oa-model"
					data-placeholder="<?php esc_attr_e( 'Модель', 'olamauto' ); ?>">
				<option></option>
				<?php foreach ( $models as $model ) { ?>
					<option value="<?php echo esc_html( $model->model ); ?>">
						<?php echo esc_html( $model->model ); ?>
					</option>
				<?php } ?>
			</select>
			<?php
		}
	}

	/**
	 * Show state select.
	 *
	 * @return void
	 */
	public static function show_state_select(): void {
		$select_options = [
			__( 'Отличное', 'olamauto' ),
			__( 'Хорошее', 'olamauto' ),
			__( 'Удовлетворительное', 'olamauto' ),
			__( 'Неудовлетворительное', 'olamauto' ),
		];

		?>
		<select
				class="state"
				name="oa_state"
				id="oa-state"
				data-placeholder="<?php esc_attr_e( 'Состояние', 'olamauto' ); ?>">
			<option></option>
			<?php foreach ( $select_options as $option ) { ?>
				<option value="<?php echo esc_html( $option ); ?>">
					<?php echo esc_html( $option ); ?>
				</option>
			<?php } ?>
		</select>
		<?php
	}

	/**
	 * Show transmission select.
	 *
	 * @return void
	 */
	public static function show_transmission_select(): void {
		$select_options = [
			__( 'Автомат', 'olamauto' ),
			__( 'Механика', 'olamauto' ),
			__( 'Вариатор', 'olamauto' ),
			__( 'Робот', 'olamauto' ),
		];

		?>
		<select
				class="transmission"
				name="oa_transmission"
				id="oa-transmission"
				data-placeholder="<?php esc_attr_e( 'Трансмиссия', 'olamauto' ); ?>">
			<option></option>
			<?php foreach ( $select_options as $option ) { ?>
				<option value="<?php echo esc_html( $option ); ?>">
					<?php echo esc_html( $option ); ?>
				</option>
			<?php } ?>
		</select>
		<?php
	}
}
