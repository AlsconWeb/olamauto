<?php
/**
 * Carbone Fields init class.
 *
 * @package march/theme
 */

namespace Olamauto;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarbonFields file.
 */
class CarbonFields {
	/**
	 * CarbonFields construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'load_carbon_fields' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'register_theme_options' ] );
	}

	/**
	 * Load main class Carbon Field.
	 *
	 * @return void
	 */
	public function load_carbon_fields(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Theme options.
	 *
	 * @return void
	 */
	public function register_theme_options(): void {
		$basic_options_container = Container::make( 'theme_options', __( 'Основные параметры', 'olamauto' ) )
		                                    ->add_fields(
			                                    [
				                                    Field::make( 'header_scripts', 'oa_header_script', __( 'Скрипт в хедере', 'olamauto' ) ),
				                                    Field::make( 'footer_scripts', 'oa_footer_script', __( 'Скрипт в футере', 'olamauto' ) ),
			                                    ]
		                                    );

		Container::make( 'theme_options', __( 'Хедер', 'olamauto' ) )
		         ->set_page_parent( $basic_options_container )
		         ->add_fields(
			         [
				         Field::make( 'image', 'oa_logo', __( 'Лого', 'olamauto' ) ),
				         Field::make( 'image', 'oa_logo_repost', __( 'Лого репост', 'olamauto' ) ),
				         Field::make( 'separator', 'oa_separator_social', __( 'Социальные сети', 'olamauto' ) ),
				         Field::make( 'complex', 'oa_social', __( 'Соц.', 'olamauto' ) )
				              ->add_fields(
					              [
						              Field::make( 'text', 'name', __( 'Названеи', 'olamauto' ) ),
						              Field::make( 'text', 'url', __( 'Ссылка', 'olamauto' ) ),

					              ]
				              ),
			         ]
		         );

		Container::make( 'theme_options', __( 'Футер', 'olamauto' ) )
		         ->set_page_parent( $basic_options_container )
		         ->add_fields(
			         [
				         Field::make( 'rich_text', 'oa_copyright', __( 'Копират', 'olamauto' ) ),
				         Field::make( 'image', 'oa_footer_logo_repost', __( 'Лого репост', 'olamauto' ) ),
			         ]
		         );

		Container::make( 'theme_options', __( 'Главная страница', 'olamauto' ) )
		         ->set_page_parent( $basic_options_container )
		         ->add_fields(
			         [
				         Field::make( 'rich_text', 'ao_mian_text_block' . crb_get_i18n_suffix(), __( 'Главный блок', 'olamauto' ) ),
				         Field::make( 'separator', 'oa_separator_price' . crb_get_i18n_suffix(), __( 'Форма цена вашего авто', 'olamauto' ) ),
				         Field::make( 'rich_text', 'ao_result_price' . crb_get_i18n_suffix(), __( 'Текст оценки авто', 'olamauto' ) ),
				         Field::make( 'separator', 'oa_separator_lead_form' . crb_get_i18n_suffix(), __( 'Форма цена вашего авто', 'olamauto' ) ),
				         Field::make( 'rich_text', 'ao_lead_form' . crb_get_i18n_suffix(), __( 'Текст на лидформе', 'olamauto' ) ),
				         Field::make( 'separator', 'oa_separator_tnx_form' . crb_get_i18n_suffix(), __( 'Успешная отправка', 'olamauto' ) ),
				         Field::make( 'rich_text', 'ao_tnx_page' . crb_get_i18n_suffix(), __( 'Текст успешкой отправки формы', 'olamauto' ) ),
			         ]
		         );
	}
}
