<?php
/**
 * Lead forms handler.
 *
 * @package olamauto/theme
 */

namespace Olamauto;

use Olamauto\Email\AdminEmail;

/**
 * LeadForms class file.
 */
class LeadForms {
	/**
	 * Change action and nonce name.
	 */
	public const OA_MODEL_CHANGE_ACTION_NAME = 'oa_modal_change_action';

	/**
	 * Send you car nonce and action.
	 */
	public const OA_SEND_YUO_CAR_ACTION_NAME = 'oa_send_you_car_action';

	/**
	 * Send lead car.
	 */
	public const OA_SEND_LEAD_CAR_ACTION_NAME = 'oa_send_lead_car_action';

	/**
	 * LeadForms construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_ajax_' . self::OA_MODEL_CHANGE_ACTION_NAME, [ $this, 'get_year_by_model' ] );
		add_action( 'wp_ajax_nopriv_' . self::OA_MODEL_CHANGE_ACTION_NAME, [ $this, 'get_year_by_model' ] );

		add_action( 'wp_ajax_' . self::OA_SEND_YUO_CAR_ACTION_NAME, [ $this, 'get_price_by_model_and_year' ] );
		add_action( 'wp_ajax_nopriv_' . self::OA_SEND_YUO_CAR_ACTION_NAME, [ $this, 'get_price_by_model_and_year' ] );

		add_action( 'wp_ajax_' . self::OA_SEND_LEAD_CAR_ACTION_NAME, [ $this, 'send_lead_form' ] );
		add_action( 'wp_ajax_nopriv_' . self::OA_SEND_LEAD_CAR_ACTION_NAME, [ $this, 'send_lead_form' ] );
	}

	/**
	 * Get year by modal.
	 *
	 * @return void
	 */
	public function get_year_by_model(): void {
		$nonce = ! empty( $_POST['changeModelNonce'] ) ? filter_var( wp_unslash( $_POST['changeModelNonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::OA_MODEL_CHANGE_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'olamauto' ) ] );
		}

		$model = ! empty( $_POST['model'] ) ? filter_var( wp_unslash( $_POST['model'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( $model ) ) {
			wp_send_json_error( [ 'message' => __( 'Модель пустая', 'olamauto' ) ] );
		}

		$years = DBHelper::get_year_model( $model );

		if ( ! empty( $years ) ) {
			$options = '';
			foreach ( $years as $year ) {
				$options .= '<option value="' . $year->year . '">' . $year->year . '</option>';
			}

			wp_send_json_success( [ 'options' => $options ] );
		}

		wp_send_json_error( [ 'message' => __( 'Прозошла ошибка попробуйте мозже', 'olamauto' ) ] );
	}

	/**
	 * Send you car form handler, get price by model and year.
	 *
	 * @return void
	 */
	public function get_price_by_model_and_year(): void {
		$nonce = ! empty( $_POST['yuoCarFromNonce'] ) ? filter_var( wp_unslash( $_POST['yuoCarFromNonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::OA_SEND_YUO_CAR_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'olamauto' ) ] );
		}

		$model = ! empty( $_POST['model'] ) ? filter_var( wp_unslash( $_POST['model'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( $model ) ) {
			wp_send_json_error( [ 'message' => __( 'Модель не передана', 'olamauto' ) ] );
		}

		$year = ! empty( $_POST['year'] ) ? filter_var( wp_unslash( $_POST['year'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $year ) ) {
			wp_send_json_error( [ 'message' => __( 'Год не был передан', 'olamauto' ) ] );
		}

		$result = DBHelper::get_car_price( $model, $year );

		if ( ! empty( $result ) ) {
			wp_send_json_success( [ 'price' => $result[0]->price ] );
		}

		wp_send_json_error( [ 'message' => __( 'Цена не найдена на ваш автомобиль', 'olamauto' ) ] );
	}

	/**
	 * Send lead form ajax handler.
	 *
	 * @return void
	 */
	public function send_lead_form(): void {
		$nonce = ! empty( $_POST['sendLeadNonce'] ) ? filter_var( wp_unslash( $_POST['sendLeadNonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::OA_SEND_LEAD_CAR_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'olamauto' ) ] );
		}

		$full_name = ! empty( $_POST['fullName'] ) ? filter_var( wp_unslash( $_POST['fullName'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( $full_name ) ) {
			wp_send_json_error( [ 'message' => __( 'Поле ФИО пустое', 'olamauto' ) ] );
		}

		$email = ! empty( $_POST['email'] ) ? filter_var( wp_unslash( $_POST['email'] ), FILTER_SANITIZE_EMAIL ) : null;

		if ( empty( $email ) ) {
			wp_send_json_error( [ 'message' => __( 'Емеил не верный или не передан', 'olamauto' ) ] );
		}

		$phone = ! empty( $_POST['leadPhone'] ) ? filter_var( wp_unslash( $_POST['leadPhone'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $phone ) ) {
			wp_send_json_error( [ 'message' => __( 'Телефон не валидный или не передан', 'olamauto' ) ] );
		}

		$model        = ! empty( $_POST['model'] ) ? filter_var( wp_unslash( $_POST['model'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : '';
		$year         = ! empty( $_POST['year'] ) ? filter_var( wp_unslash( $_POST['year'] ), FILTER_SANITIZE_NUMBER_INT ) : '';
		$color        = ! empty( $_POST['color'] ) ? filter_var( wp_unslash( $_POST['color'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : '';
		$mileage      = ! empty( $_POST['mileage'] ) ? filter_var( wp_unslash( $_POST['mileage'] ), FILTER_SANITIZE_NUMBER_INT ) : '';
		$state        = ! empty( $_POST['state'] ) ? filter_var( wp_unslash( $_POST['state'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : '';
		$transmission = ! empty( $_POST['transmission'] ) ? filter_var( wp_unslash( $_POST['transmission'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : '';
		$price        = ! empty( $_POST['price'] ) ? filter_var( wp_unslash( $_POST['price'] ), FILTER_SANITIZE_NUMBER_INT ) : '';

		AdminEmail::send_lead_form(
			[
				'full_name'    => $full_name,
				'email'        => $email,
				'phone'        => $phone,
				'model'        => $model,
				'year'         => $year,
				'color'        => $color,
				'mileage'      => $mileage,
				'state'        => $state,
				'transmission' => $transmission,
				'price'        => $price,
				'sendTo'       => 'uzolamavto@gmail.com',
			]
		);

		wp_send_json_success();
	}

}
