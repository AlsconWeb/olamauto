<?php
/**
 * Main class theme olamauto.
 *
 * @package olamauto/theme
 */

namespace Olamauto;

/**
 * Main class file.
 */
class Main {
	/**
	 * Theme version.
	 */
	public const OA_THEME_VERSION = '1.0.0';

	/**
	 * Cart table name.
	 */
	public const OA_CARS_TABLE_NAME = 'cars';

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		new CarbonFields();
		new CarsPage();
		new LeadForms();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_style_and_scripts' ] );
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'after_setup_theme', [ $this, 'add_cars_table' ] );

		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
	}

	/**
	 * Add style and scripts.
	 *
	 * @return void
	 */
	public function add_style_and_scripts(): void {
		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		wp_enqueue_script( 'oa_select2', $url . '/assets/js/select2.min.js', [ 'jquery' ], self::OA_THEME_VERSION, true );
		wp_enqueue_script( 'oa_build', $url . '/assets/js/build' . $min . '.js', [ 'jquery' ], self::OA_THEME_VERSION, true );
		wp_enqueue_script( 'oa_main', $url . '/assets/js/main' . $min . '.js', [ 'jquery' ], self::OA_THEME_VERSION, true );

		wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0', false );
		wp_enqueue_script( 'respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2', false );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

		wp_enqueue_style( 'google-font', '//fonts.googleapis.com/css2?family=Montserrat:wght@500;700&amp;display=swap', '', '1.0' );
		wp_enqueue_style( 'oa_bootstrap_grid', $url . '/assets/css/bootstrap-grid' . $min . '.css', '', self::OA_THEME_VERSION );
		wp_enqueue_style( 'oa_select2', $url . '/assets/css/select2.min.css', '', self::OA_THEME_VERSION );
		wp_enqueue_style( 'oa_main', $url . '/assets/css/main' . $min . '.css', '', self::OA_THEME_VERSION );

		wp_localize_script(
			'oa_main',
			'oaMainObject',
			[
				'ajaxUrl'           => admin_url( 'admin-ajax.php' ),
				'changeModelAction' => LeadForms::OA_MODEL_CHANGE_ACTION_NAME,
				'changeModelNonce'  => wp_create_nonce( LeadForms::OA_MODEL_CHANGE_ACTION_NAME ),
				'youCarFormAction'  => LeadForms::OA_SEND_YUO_CAR_ACTION_NAME,
				'youCarFormNonce'   => wp_create_nonce( LeadForms::OA_SEND_YUO_CAR_ACTION_NAME ),
				'sendLeadAction'    => LeadForms::OA_SEND_LEAD_CAR_ACTION_NAME,
				'sendLeadNonce'     => wp_create_nonce( LeadForms::OA_SEND_LEAD_CAR_ACTION_NAME ),
			]
		);
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {

		// add custom logo.
		add_theme_support( 'custom-logo' );

		// add menu support.
		add_theme_support( 'menus' );

		register_nav_menus(
			[
				'top_bar_menu' => __( 'Social network', 'march' ),
			]
		);

		// post thumbnail.
		add_theme_support( 'post-thumbnails' );

		// add images sizes.
		add_image_size( 'ma-coupon-image', 580, 760, [ 'top', 'center' ] );
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Create cars table.
	 *
	 * @return void
	 */
	public function add_cars_table(): void {
		$cars_table = get_option( 'oa_cars_table', false );

		if ( ! $cars_table ) {
			global $wpdb;

			$table_name = $wpdb->prefix . self::OA_CARS_TABLE_NAME;

			$sql = "CREATE TABLE `$table_name` 
					(
					    `id` INT NOT NULL AUTO_INCREMENT , 
					    `model` VARCHAR(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL , 
					    `year` INT(100) NULL , 
					    `price` INT NULL , PRIMARY KEY (`id`)
					) ENGINE = InnoDB;";

			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			dbDelta( $sql );

			add_option( 'oa_cars_table', true );
		}
	}
}
