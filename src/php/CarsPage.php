<?php
/**
 * Add cars page.
 *
 * @package olamauto/theme
 */

namespace Olamauto;

/**
 * CarsPage class file.
 */
class CarsPage {

	/**
	 * Add car action name.
	 */
	public const OA_ADD_CAR_ACTION_NAME = 'add_car_in_table';

	/**
	 * Delete car action name.
	 */
	public const OA_DELETE_CAR_ACTION_NAME = 'delete_car_in_table';

	/**
	 * CarsPage construct.
	 */
	public function __construct() {
		add_action( 'admin_menu', [ $this, 'add_cars_page_in_menu' ] );
		add_action( 'admin_enqueue_scripts', [ $this, 'add_admin_scripts_and_style' ] );

		add_action( 'wp_ajax_' . self::OA_ADD_CAR_ACTION_NAME, [ $this, 'add_car' ] );
		add_action( 'wp_ajax_nopriv_' . self::OA_ADD_CAR_ACTION_NAME, [ $this, 'add_car' ] );

		add_action( 'wp_ajax_' . self::OA_DELETE_CAR_ACTION_NAME, [ $this, 'delete_car' ] );
		add_action( 'wp_ajax_nopriv_' . self::OA_DELETE_CAR_ACTION_NAME, [ $this, 'delete_car' ] );
	}

	/**
	 * Add script and style.
	 *
	 * @param string $hooks Hooks name.
	 *
	 * @return void
	 */
	public function add_admin_scripts_and_style( $hooks ) {
		if ( 'toplevel_page_cars' === $hooks ) {
			wp_enqueue_style( 'bootstrap', '//cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css', '', '5.3.1' );

			wp_enqueue_script( 'oa-admin-main', get_stylesheet_directory_uri() . '/assets/js/admin-main.js', [ 'jquery' ], Main::OA_THEME_VERSION, true );

			wp_localize_script(
				'oa-admin-main',
				'oaAdminMainObj',
				[
					'ajaxUrl'         => admin_url( 'admin-ajax.php' ),
					'addCarAction'    => self::OA_ADD_CAR_ACTION_NAME,
					'addCarNonce'     => wp_create_nonce( self::OA_ADD_CAR_ACTION_NAME ),
					'deleteCarAction' => self::OA_DELETE_CAR_ACTION_NAME,
					'deleteCarNonce'  => wp_create_nonce( self::OA_DELETE_CAR_ACTION_NAME ),
				]
			);
		}
	}

	/**
	 * Add cars page in menu.
	 *
	 * @return void
	 */
	public function add_cars_page_in_menu(): void {
		add_menu_page(
			__( 'Cars', 'olamauto' ),
			__( 'Cars', 'olamauto' ),
			'manage_options',
			'cars',
			[ $this, 'show_cars_page' ],
			'dashicons-car',
			30
		);
	}

	/**
	 * Add car ajax handler.
	 *
	 * @return void
	 */
	public function add_car(): void {
		$nonce = ! empty( $_POST['addCarNonce'] ) ? filter_var( wp_unslash( $_POST['addCarNonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::OA_ADD_CAR_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'olamauto' ) ] );
		}

		$model = ! empty( $_POST['model'] ) ? filter_var( wp_unslash( $_POST['model'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( empty( $model ) ) {
			wp_send_json_error( [ 'message' => __( 'Модель авто пустая', 'olamauto' ) ] );
		}

		$year = ! empty( $_POST['year'] ) ? filter_var( wp_unslash( $_POST['year'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $year ) ) {
			wp_send_json_error( [ 'message' => __( 'Год выпуска авто пустой', 'olamauto' ) ] );
		}

		$price = ! empty( $_POST['price'] ) ? filter_var( wp_unslash( $_POST['price'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $price ) ) {
			wp_send_json_error( [ 'message' => __( 'Цена авто авто пустой', 'olamauto' ) ] );
		}

		$result = DBHelper::add_cart(
			[
				'model' => trim( $model ),
				'year'  => trim( $year ),
				'price' => trim( $price ),
			]
		);

		if ( ! isset( $result['error'] ) ) {
			wp_send_json_success( $result );
		}
	}

	/**
	 * Delete car ajax handler.
	 *
	 * @return void
	 */
	public function delete_car(): void {
		$nonce = ! empty( $_POST['deleteCarNonce'] ) ? filter_var( wp_unslash( $_POST['deleteCarNonce'] ), FILTER_SANITIZE_FULL_SPECIAL_CHARS ) : null;

		if ( ! wp_verify_nonce( $nonce, self::OA_DELETE_CAR_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Не верный одноразовый код', 'olamauto' ) ] );
		}

		$car_id = ! empty( $_POST['id'] ) ? filter_var( wp_unslash( $_POST['id'] ), FILTER_SANITIZE_NUMBER_INT ) : null;

		if ( empty( $car_id ) ) {
			wp_send_json_error( [ 'message' => __( 'ID не передан', 'olamauto' ) ] );
		}

		$result = DBHelper::delete_car( $car_id );

		if ( $result ) {
			wp_send_json_success();
		}

		wp_send_json_error( [ 'message' => __( 'Авто не было удалено' ) ] );
	}

	/**
	 * Show cars page.
	 *
	 * @return void
	 */
	public function show_cars_page(): void {

		$cars = DBHelper::get_all_cars();
		?>
		<div class="container">
			<div class="row">
				<div class="col mb-3">
					<h1>Cars</h1>
				</div>
			</div>
			<div class="row">
				<div class="col">
					<table class="table table-striped-columns">
						<thead>
						<tr>
							<th scope="col">
								<?php esc_attr_e( 'ID', 'olamauto' ); ?>
							</th>
							<th scope="col">
								<?php esc_attr_e( 'Модель', 'olamauto' ); ?>
							</th>
							<th scope="col">
								<?php esc_attr_e( 'Год', 'olamauto' ); ?>
							</th>
							<th scope="col">
								<?php esc_attr_e( 'Цена', 'olamauto' ); ?>
							</th>
							<th scope="col">
								<?php esc_attr_e( 'Действие', 'olamauto' ); ?>
							</th>
						</tr>
						</thead>
						<tbody>
						<?php
						if ( ! empty( $cars ) ) {
							foreach ( $cars as $car ) {
								?>
								<tr>

									<th><?php echo esc_html( $car->id ); ?></th>
									<td><?php echo esc_html( $car->model ); ?></td>
									<td><?php echo esc_html( $car->year ); ?></td>
									<td><?php echo esc_html( $car->price ); ?></td>
									<td>
										<a
												class="icon-link delete"
												data-id="<?php echo esc_attr( $car->id ); ?>"
												href="#">
											<?php esc_attr_e( 'Удалить', 'olamauto' ); ?>
										</a>
									</td>
								</tr>
								<?php
							}
						}
						?>
						<form action="" method="post" id="add_car">
							<tr>
								<th>#</th>
								<td>
									<div>
										<label for="oa-model" class="form-label">
											<?php esc_attr_e( 'Model', 'olamauto' ); ?>
										</label>
										<input
												type="text"
												class="form-control"
												id="oa-model"
												name="oa_add_car[model]"
												required
												placeholder="<?php esc_attr_e( 'Model', 'olamauto' ); ?>">
									</div>
								</td>
								<td>
									<div>
										<label for="oa-year" class="form-label">
											<?php esc_attr_e( 'Year', 'olamauto' ); ?>
										</label>
										<input
												type="text"
												class="form-control"
												id="oa-year"
												name="oa_add_car[year]"
												required
												placeholder="<?php esc_attr_e( 'Year', 'olamauto' ); ?>">
									</div>
								</td>
								<td>
									<div>
										<label for="oa-price" class="form-label">
											<?php esc_attr_e( 'Price', 'olamauto' ); ?>
										</label>
										<input
												type="text"
												class="form-control"
												id="oa-price"
												name="oa_add_car[price]"
												required
												placeholder="<?php esc_attr_e( 'Price', 'olamauto' ); ?>">
									</div>
								</td>
								<td>
									<input
											type="submit"
											class="button button-primary"
											value="<?php esc_attr_e( 'Add car' ); ?>">
								</td>
							</tr>
						</form>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<?php
	}
}
