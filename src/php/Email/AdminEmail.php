<?php
/**
 * Send Admin email.
 *
 * @package olamauto/theme
 */

namespace Olamauto\Email;

/**
 * AdminEmail class file.
 */
class AdminEmail extends BaseEmail {
	/**
	 * Send register validation.
	 *
	 * @param array $arg Arguments.
	 *
	 * @return void
	 */
	public static function send_lead_form( array $arg ): void {
		ob_start();
		?>
		<tr>
			<td>
				<hr>
				<h3><?php esc_html_e( 'Лид с формы', 'olamauto' ); ?></h3>
				<p>
					<?php esc_attr_e( 'Ф.И.О:', 'olamauto' ) . ' ' . $arg['full_name']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Электронная почта:', 'olamauto' ) . ' ' . $arg['email']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Телефон:', 'olamauto' ) . ' ' . $arg['phone']; ?>
				</p>
				<hr>
				<h3><?php esc_attr_e( 'Авто на просчет' ); ?></h3>
				<p>
					<?php esc_attr_e( 'Модель:', 'olamauto' ) . ' ' . $arg['model']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Год выпуска:', 'olamauto' ) . ' ' . $arg['year']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Цвет кузова:', 'olamauto' ) . ' ' . $arg['color']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Пробег:', 'olamauto' ) . ' ' . $arg['mileage']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Состояние:', 'olamauto' ) . ' ' . $arg['state']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Тип коробки передач:', 'olamauto' ) . ' ' . $arg['transmission']; ?>
				</p>
				<p>
					<?php esc_attr_e( 'Цена на которая предложила база данных:', 'olamauto' ) . ' ' . $arg['price']; ?>
				</p>
			</td>
		</tr>
		<?php
		$data = [
			'title'   => __( 'Лид с формы на просчет авто', 'olamauto' ),
			'content' => ob_get_clean(),
		];

		$content = self::get_content( $data );
		$subject = __( 'Лид с формы на просчет авто', 'olamauto' );
		$email   = $arg['sendTo'];

		self::send( $subject, $content, $email );
	}
}
