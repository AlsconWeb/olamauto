<?php
/**
 * Db helper class.
 *
 * @package olamauto/theme
 */

namespace Olamauto;

use stdClass;

/**
 * DBHelper class.
 */
class DBHelper {

	/**
	 * DBHelper construct.
	 */
	public function __construct() {

	}

	/**
	 * Add car to bd.
	 *
	 * @param array $data Data, array keys: model, year, price.
	 *
	 * @return array
	 */
	public static function add_cart( array $data ): array {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::OA_CARS_TABLE_NAME;

		// phpcs:disable
		$result = $wpdb->insert( $table_name, $data, [ '%s', '%d', '%d' ] );
		// phpcs:enable

		if ( $result ) {
			return [ 'id' => $wpdb->insert_id ];
		}

		return [ 'error' => $wpdb->last_error ];
	}

	/**
	 * Get all cars.
	 *
	 * @return array|stdClass[]
	 */
	public static function get_all_cars() {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::OA_CARS_TABLE_NAME;

		$sql = "SELECT * FROM $table_name";

		// phpcs:disable
		$result = $wpdb->get_results( $sql );
		// phpcs:enable

		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Delete car.
	 *
	 * @param int $car_id Car id.
	 *
	 * @return bool
	 */
	public static function delete_car( int $car_id ) {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::OA_CARS_TABLE_NAME;
		// phpcs:disable
		$result = $wpdb->delete( $table_name, [ 'id' => $car_id ], [ '%d' ] );
		// phpcs:enable

		if ( empty( $result ) ) {
			return false;
		}

		return true;
	}

	/**
	 * Get unique model.
	 *
	 * @return array|stdClass[]
	 */
	public static function get_unique_model() {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::OA_CARS_TABLE_NAME;

		$sql = "SELECT DISTINCT model FROM $table_name;";
		// phpcs:disable
		$result = $wpdb->get_results( $sql );
		// phpcs:enable

		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Get year by model.
	 *
	 * @param string $model Model name.
	 *
	 * @return array|stdClass[]
	 */
	public static function get_year_model( string $model ) {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::OA_CARS_TABLE_NAME;

		$sql = "SELECT year FROM $table_name WHERE model = %s;";

		// phpcs:disable
		$result = $wpdb->get_results(
			$wpdb->prepare( $sql, $model )
		);
		// phpcs:enable

		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}

	/**
	 * Get car price.
	 *
	 * @param string $model Car model.
	 * @param int    $year  Car year.
	 *
	 * @return array|stdClass[]
	 */
	public static function get_car_price( string $model, int $year ) {
		global $wpdb;

		$table_name = $wpdb->prefix . Main::OA_CARS_TABLE_NAME;

		$sql = "SELECT price FROM $table_name WHERE `model` = %s AND `year` = %d";

		// phpcs:disable
		$result = $wpdb->get_results(
			$wpdb->prepare( $sql, $model, $year )
		);
		// phpcs:enable

		if ( ! empty( $result ) ) {
			return $result;
		}

		return [];
	}
}
