<?php
/**
 * Header template.
 *
 * @package olamauto/theme
 */

$logo_id  = carbon_get_theme_option( 'oa_logo' );
$logo_url = wp_get_attachment_image_url( $logo_id, 'full' );

$repost_logo_id  = carbon_get_theme_option( 'oa_logo_repost' );
$repost_logo_url = wp_get_attachment_image_url( $repost_logo_id, 'full' );
$social_media    = carbon_get_theme_option( 'oa_social' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
</head>
<body>
<header>
	<div class="container">
		<div class="row align-items-center">
			<div class="col">
				<div class="logo">
					<?php if ( ! empty( $logo_id ) ) { ?>
						<a href="https://promo.olamavto.uz/">
							<img src="<?php echo esc_url( $logo_url ); ?>" alt="logo-olamauto">
						</a>
					<?php } ?>
					<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/x.svg' ); ?>" alt="X">
					<?php if ( ! empty( $repost_logo_id ) ) { ?>
						<a href="http://repost.uz/">
							<img src="<?php echo esc_url( $repost_logo_url ); ?>" alt="repost-logo">
						</a>
					<?php } ?>
				</div>
			</div>
			<div class="col-auto">
				<?php if ( ! empty( $social_media ) ) { ?>
					<ul class="soc">
						<?php foreach ( $social_media as $media ) { ?>
							<li class="icon-<?php echo esc_attr( $media['name'] ?? '' ); ?>">
								<a href="<?php echo esc_url( $media['url'] ?? '' ); ?>"></a>
							</li>
						<?php } ?>
					</ul>
				<?php } ?>
			</div>
			<div class="col-auto">
				<?php do_action( 'wpml_add_language_selector' ); ?>
			</div>
		</div>
	</div>
</header>
